EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 1500 1900
$Sheet
S 5750 3250 550  350 
U 5D9538F4
F0 "System On Module" 50
F1 "file5D9538F3.sch" 50
$EndSheet
$Sheet
S 4300 3150 650  350 
U 5D953969
F0 "Power" 50
F1 "file5D953968.sch" 50
$EndSheet
$Sheet
S 4300 3750 850  400 
U 5DA2B136
F0 "Debug" 50
F1 "file5DA2B135.sch" 50
$EndSheet
$Sheet
S 7100 2500 600  400 
U 5DAA64E7
F0 "Sheet5DAA64E6" 50
F1 "IO.sch" 50
$EndSheet
$Sheet
S 7100 3150 700  500 
U 5DBB1859
F0 "Cellular Module" 50
F1 "file5DBB1858.sch" 50
$EndSheet
$Sheet
S 4300 2450 500  450 
U 5E1F1922
F0 "Display" 50
F1 "file5E1F1921.sch" 50
$EndSheet
$Sheet
S 7100 3950 750  550 
U 5E66DF99
F0 "USB Hub" 50
F1 "file5E66DF98.sch" 50
$EndSheet
$EndSCHEMATC
