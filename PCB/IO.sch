EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_C_Receptacle_USB2.0 J7
U 1 1 5DD9CD57
P 1400 3450
F 0 "J7" H 1507 4317 50  0000 C CNN
F 1 "Dual Role User Port" H 1507 4226 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_Palconn_UTC16-G" H 1550 3450 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1550 3450 50  0001 C CNN
	1    1400 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5DD9CD62
P 1250 4550
F 0 "#PWR0113" H 1250 4300 50  0001 C CNN
F 1 "GND" H 1255 4377 50  0000 C CNN
F 2 "" H 1250 4550 50  0001 C CNN
F 3 "" H 1250 4550 50  0001 C CNN
	1    1250 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 4350 1400 4450
Wire Wire Line
	1400 4450 1250 4450
Wire Wire Line
	1250 4450 1250 4550
Wire Wire Line
	1100 4350 1100 4450
Wire Wire Line
	1100 4450 1250 4450
Connection ~ 1250 4450
NoConn ~ 2000 3950
NoConn ~ 2000 4050
$Comp
L Interface_USB:TUSB320 U1
U 1 1 5DD9EAFA
P 4950 3300
F 0 "U1" H 5100 3750 50  0000 C CNN
F 1 "TUSB320" H 4750 3750 50  0000 C CNN
F 2 "Package_DFN_QFN:Texas_X2QFN-12_1.6x1.6mm_P0.4mm" H 5150 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tusb320.pdf" H 4950 3300 50  0001 C CNN
	1    4950 3300
	-1   0    0    1   
$EndComp
Text Notes 3700 4750 0    50   ~ 0
if (ID = 1)\nthen\nStatus = Downstream Facing Port\nSupply Power\nelse\nStatus = Upstream Facing Port\nConsume Power
Text GLabel 5750 4050 3    50   Input ~ 0
PORT_ID
Wire Wire Line
	5750 4050 5750 3750
Text GLabel 6050 2600 0    50   Input ~ 0
PORT_ID
$Comp
L Device:LED_Small D2
U 1 1 5DF7DFF5
P 6150 2600
F 0 "D2" H 6150 2395 50  0000 C CNN
F 1 "Downstream Facing Port" H 6150 2486 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6150 2600 50  0001 C CNN
F 3 "~" V 6150 2600 50  0001 C CNN
	1    6150 2600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0135
U 1 1 5DF7EB62
P 6350 3000
F 0 "#PWR0135" H 6350 2750 50  0001 C CNN
F 1 "GND" H 6355 2827 50  0000 C CNN
F 2 "" H 6350 3000 50  0001 C CNN
F 3 "" H 6350 3000 50  0001 C CNN
	1    6350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3000 6350 2850
Wire Wire Line
	6350 2600 6250 2600
$Comp
L Device:R_Small_US R9
U 1 1 5E165F06
P 6350 2750
F 0 "R9" H 6418 2796 50  0000 L CNN
F 1 "10K" H 6418 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6350 2750 50  0001 C CNN
F 3 "~" H 6350 2750 50  0001 C CNN
	1    6350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2650 6350 2600
Text GLabel 4300 3400 0    50   Input ~ 0
USB_MAIN_CC1
Text GLabel 4300 3300 0    50   Input ~ 0
USB_MAIN_CC2
Text GLabel 2200 3050 2    50   Input ~ 0
USB_MAIN_CC1
Text GLabel 2200 3150 2    50   Input ~ 0
USB_MAIN_CC2
Wire Wire Line
	2200 3050 2000 3050
Wire Wire Line
	2200 3150 2000 3150
NoConn ~ 4350 3100
NoConn ~ 4350 3000
NoConn ~ 5550 3400
NoConn ~ 5550 3500
NoConn ~ 5550 3600
Text GLabel 2300 2750 2    50   Input ~ 0
VBUS_DET
Text GLabel 2300 2650 2    50   Input ~ 0
PORT_VOLTAGE
Wire Wire Line
	2000 2850 2100 2850
Wire Wire Line
	2100 2850 2100 2750
Wire Wire Line
	2300 2750 2100 2750
Connection ~ 2100 2750
Wire Wire Line
	2100 2750 2100 2650
Text GLabel 2900 6150 2    50   Input ~ 0
PORT_VOLTAGE
$Comp
L power:+5V #PWR0136
U 1 1 5E188954
P 4950 4000
F 0 "#PWR0136" H 4950 3850 50  0001 C CNN
F 1 "+5V" V 4965 4128 50  0000 L CNN
F 2 "" H 4950 4000 50  0001 C CNN
F 3 "" H 4950 4000 50  0001 C CNN
	1    4950 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4950 4000 4950 3800
$Comp
L power:GND #PWR0137
U 1 1 5E18B384
P 5100 2650
F 0 "#PWR0137" H 5100 2400 50  0001 C CNN
F 1 "GND" V 5105 2522 50  0000 R CNN
F 2 "" H 5100 2650 50  0001 C CNN
F 3 "" H 5100 2650 50  0001 C CNN
	1    5100 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 2800 4950 2650
Wire Wire Line
	4950 2650 5100 2650
$Comp
L Jumper:SolderJumper_2_Bridged JP3
U 1 1 5E18C204
P 5950 3500
F 0 "JP3" H 5950 3705 50  0000 C CNN
F 1 "!USBC_EN" H 5950 3614 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 5950 3500 50  0001 C CNN
F 3 "~" H 5950 3500 50  0001 C CNN
	1    5950 3500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0138
U 1 1 5E18CA30
P 5950 3900
F 0 "#PWR0138" H 5950 3650 50  0001 C CNN
F 1 "GND" V 5955 3772 50  0000 R CNN
F 2 "" H 5950 3900 50  0001 C CNN
F 3 "" H 5950 3900 50  0001 C CNN
	1    5950 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3650 5950 3900
Text GLabel 4300 3600 0    50   Input ~ 0
VBUS_DET
Wire Wire Line
	2000 3650 2050 3650
Wire Wire Line
	2050 3650 2050 3550
Wire Wire Line
	2050 3550 2150 3550
Wire Wire Line
	2000 3550 2050 3550
Connection ~ 2050 3550
Wire Wire Line
	2150 3450 2050 3450
Wire Wire Line
	2000 3350 2050 3350
Wire Wire Line
	2050 3350 2050 3450
Connection ~ 2050 3450
Wire Wire Line
	2050 3450 2000 3450
$Comp
L Jumper:SolderJumper_2_Bridged JP2
U 1 1 5E1BB7F8
P 2300 3550
F 0 "JP2" H 2300 3450 50  0000 C CNN
F 1 "USB3_DP" H 2300 3400 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 2300 3550 50  0001 C CNN
F 3 "~" H 2300 3550 50  0001 C CNN
	1    2300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3550 2450 3550
$Comp
L Jumper:SolderJumper_2_Bridged JP1
U 1 1 5E1BC8FC
P 2300 3450
F 0 "JP1" H 2300 3650 50  0000 C CNN
F 1 "USB3_DN" H 2550 3550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 2300 3450 50  0001 C CNN
F 3 "~" H 2300 3450 50  0001 C CNN
	1    2300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3450 2450 3450
Text GLabel 1150 6050 0    50   Input ~ 0
PORT_ID
$Comp
L Jumper:SolderJumper_2_Bridged JP6
U 1 1 5E156A22
P 5750 3600
F 0 "JP6" H 6050 3650 50  0000 C CNN
F 1 "DRP_ID" H 5950 3500 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 5750 3600 50  0001 C CNN
F 3 "~" H 5750 3600 50  0001 C CNN
	1    5750 3600
	0    1    1    0   
$EndComp
$Comp
L Power_Management:NPC45560-H U5
U 1 1 5E17F1C2
P 1900 6150
F 0 "U5" H 1700 5700 50  0000 C CNN
F 1 "NPC45560-H" H 2200 5700 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-12-1EP_3x4mm_P0.5mm_EP1.7x3.3mm" H 1900 6700 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP45560-D.PDF" H 1900 6700 50  0001 C CNN
	1    1900 6150
	1    0    0    -1  
$EndComp
$Comp
L Power_Management:NPC45560-L U6
U 1 1 5E17FEF4
P 5000 6150
F 0 "U6" H 5000 5561 50  0000 C CNN
F 1 "NPC45560-L" H 5000 5470 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-12-1EP_3x4mm_P0.5mm_EP1.7x3.3mm" H 5000 6700 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP45560-D.PDF" H 5000 6700 50  0001 C CNN
	1    5000 6150
	1    0    0    -1  
$EndComp
Text Notes 600  7650 0    50   ~ 0
Downstream Facing Port
$Comp
L power:+5V #PWR0141
U 1 1 5E1D8925
P 2500 5400
F 0 "#PWR0141" H 2500 5250 50  0001 C CNN
F 1 "+5V" H 2515 5573 50  0000 C CNN
F 2 "" H 2500 5400 50  0001 C CNN
F 3 "" H 2500 5400 50  0001 C CNN
	1    2500 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0142
U 1 1 5E1DC0DC
P 1700 5350
F 0 "#PWR0142" H 1700 5200 50  0001 C CNN
F 1 "+5V" H 1715 5523 50  0000 C CNN
F 2 "" H 1700 5350 50  0001 C CNN
F 3 "" H 1700 5350 50  0001 C CNN
	1    1700 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5650 1700 5350
$Comp
L Device:Fuse_Small F1
U 1 1 5E1DD504
P 2200 5500
F 0 "F1" H 2200 5685 50  0000 C CNN
F 1 "DFP Overcurrent" H 2200 5594 50  0000 C CNN
F 2 "Fuse:Fuse_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2200 5500 50  0001 C CNN
F 3 "~" H 2200 5500 50  0001 C CNN
	1    2200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5650 2000 5500
Wire Wire Line
	2000 5500 2100 5500
Wire Wire Line
	1900 5650 1900 5500
Wire Wire Line
	1900 5500 2000 5500
Connection ~ 2000 5500
Wire Wire Line
	2300 5500 2500 5500
Wire Wire Line
	2500 5500 2500 5400
$Comp
L power:GND #PWR0143
U 1 1 5E1E9DF0
P 1900 6950
F 0 "#PWR0143" H 1900 6700 50  0001 C CNN
F 1 "GND" H 1905 6777 50  0000 C CNN
F 2 "" H 1900 6950 50  0001 C CNN
F 3 "" H 1900 6950 50  0001 C CNN
	1    1900 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6650 1900 6800
NoConn ~ 1400 6250
Wire Wire Line
	2400 5850 2550 5850
Wire Wire Line
	2550 5850 2550 6800
Wire Wire Line
	2550 6800 1900 6800
Connection ~ 1900 6800
Wire Wire Line
	1900 6800 1900 6950
Wire Wire Line
	2400 6450 2500 6450
Wire Wire Line
	2500 6450 2500 6350
Wire Wire Line
	2500 5950 2400 5950
Wire Wire Line
	2400 6050 2500 6050
Connection ~ 2500 6050
Wire Wire Line
	2500 6050 2500 5950
Wire Wire Line
	2400 6150 2500 6150
Connection ~ 2500 6150
Wire Wire Line
	2500 6150 2500 6050
Wire Wire Line
	2400 6250 2500 6250
Connection ~ 2500 6250
Wire Wire Line
	2500 6250 2500 6150
Wire Wire Line
	2400 6350 2500 6350
Connection ~ 2500 6350
Wire Wire Line
	2500 6350 2500 6250
Wire Notes Line
	550  7700 3550 7700
Wire Notes Line
	3550 7700 3550 5000
Wire Notes Line
	3550 5000 550  5000
Wire Notes Line
	550  5000 550  7700
Text Notes 2000 6950 0    50   ~ 0
Using built in Bleed R
Text GLabel 5400 5500 2    50   Input ~ 0
PORT_VOLTAGE
Text GLabel 4250 6050 0    50   Input ~ 0
PORT_ID
Text Notes 3700 7650 0    50   ~ 0
Upstream Facing Port
$Comp
L power:+5V #PWR0144
U 1 1 5E213637
P 4800 5350
F 0 "#PWR0144" H 4800 5200 50  0001 C CNN
F 1 "+5V" H 4815 5523 50  0000 C CNN
F 2 "" H 4800 5350 50  0001 C CNN
F 3 "" H 4800 5350 50  0001 C CNN
	1    4800 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5650 4800 5350
Wire Wire Line
	5100 5650 5100 5500
Wire Wire Line
	5100 5500 5200 5500
Wire Wire Line
	5000 5650 5000 5500
Wire Wire Line
	5000 5500 5100 5500
Connection ~ 5100 5500
$Comp
L power:GND #PWR0145
U 1 1 5E213653
P 5000 6950
F 0 "#PWR0145" H 5000 6700 50  0001 C CNN
F 1 "GND" H 5005 6777 50  0000 C CNN
F 2 "" H 5000 6950 50  0001 C CNN
F 3 "" H 5000 6950 50  0001 C CNN
	1    5000 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6650 5000 6800
NoConn ~ 4500 6250
Wire Wire Line
	5500 5850 5650 5850
Wire Wire Line
	5650 5850 5650 6800
Wire Wire Line
	5650 6800 5000 6800
Connection ~ 5000 6800
Wire Wire Line
	5000 6800 5000 6950
Wire Wire Line
	5500 6450 5600 6450
Wire Wire Line
	5600 6450 5600 6350
Wire Wire Line
	5600 5950 5500 5950
Wire Wire Line
	5500 6050 5600 6050
Connection ~ 5600 6050
Wire Wire Line
	5600 6050 5600 5950
Wire Wire Line
	5500 6150 5600 6150
Connection ~ 5600 6150
Wire Wire Line
	5600 6150 5600 6050
Wire Wire Line
	5500 6250 5600 6250
Connection ~ 5600 6250
Wire Wire Line
	5600 6250 5600 6150
Wire Wire Line
	5500 6350 5600 6350
Connection ~ 5600 6350
Wire Wire Line
	5600 6350 5600 6250
Wire Notes Line
	3650 7700 6650 7700
Wire Notes Line
	6650 7700 6650 5000
Wire Notes Line
	6650 5000 3650 5000
Wire Notes Line
	3650 5000 3650 7700
Text Notes 5750 6450 0    50   ~ 0
Using built in Bleed R
$Comp
L Device:D_Small D3
U 1 1 5E23D089
P 2750 6150
F 0 "D3" H 2750 5945 50  0000 C CNN
F 1 "DFP Backfeed" H 2750 6036 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2750 6150 50  0001 C CNN
F 3 "~" V 2750 6150 50  0001 C CNN
	1    2750 6150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 6150 2650 6150
Wire Wire Line
	2850 6150 2900 6150
Text Notes 2600 6350 0    50   ~ 0
Do not backfeed Switch
Text Notes 5250 7650 0    50   ~ 0
if (ID = 1)\nthen\nStatus = Downstream Facing Port\nSupply Power\nelse\nStatus = Upstream Facing Port\nConsume Power
Text Notes 2150 7650 0    50   ~ 0
if (ID = 1)\nthen\nStatus = Downstream Facing Port\nSupply Power\nelse\nStatus = Upstream Facing Port\nConsume Power
$Comp
L SparkFun-PowerSymbols:V_USB #SUPPLY0102
U 1 1 5E28046D
P 5900 6050
F 0 "#SUPPLY0102" H 5950 6050 45  0001 L BNN
F 1 "V_USB" H 5900 6326 45  0000 C CNN
F 2 "XXX-00000" H 5900 6231 60  0000 C CNN
F 3 "" H 5900 6050 60  0001 C CNN
	1    5900 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6150 5900 6050
$Comp
L Device:D_Small D4
U 1 1 5E2A48F8
P 5300 5500
F 0 "D4" H 5300 5705 50  0000 C CNN
F 1 "UFP Backfeed" H 5300 5614 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5300 5500 50  0001 C CNN
F 3 "~" V 5300 5500 50  0001 C CNN
	1    5300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 6150 5900 6150
$Comp
L Connector:TestPoint TP1
U 1 1 5E2B8621
P 2100 2550
F 0 "TP1" H 2158 2668 50  0000 L CNN
F 1 "VUSB" H 2158 2577 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2300 2550 50  0001 C CNN
F 3 "~" H 2300 2550 50  0001 C CNN
	1    2100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2650 2100 2650
Connection ~ 2100 2650
Wire Wire Line
	2100 2650 2100 2550
Text Notes 3700 4900 0    50   ~ 0
Dual Role Port Controller
Wire Notes Line
	3650 4950 6650 4950
Wire Notes Line
	6650 4950 6650 2250
Wire Notes Line
	6650 2250 3650 2250
Wire Notes Line
	3650 2250 3650 4950
Wire Wire Line
	4300 3300 4350 3300
Wire Wire Line
	4300 3400 4350 3400
Wire Wire Line
	4300 3600 4350 3600
Wire Wire Line
	5950 3100 5950 3350
Wire Wire Line
	5750 3450 5750 3300
Wire Wire Line
	5750 3300 5550 3300
Wire Wire Line
	5550 3100 5950 3100
Text Notes 600  4900 0    50   ~ 0
USB Type C Port
Wire Notes Line
	550  4950 3550 4950
Wire Notes Line
	3550 4950 3550 2250
Wire Notes Line
	3550 2250 550  2250
Wire Notes Line
	550  2250 550  4950
Text Notes 2050 3900 0    50   ~ 0
Solder Jumpers to ensure the ability\nto cut data lines if fault found
Text Notes 6750 6150 0    50   ~ 0
Internal USB Port
$Comp
L Connector_Generic:Conn_01x04 J11
U 1 1 5E4DB69C
P 6950 5600
F 0 "J11" H 6868 5175 50  0000 C CNN
F 1 "Conn_01x04" H 6868 5266 50  0000 C CNN
F 2 "Connector_FFC-FPC:Molex_200528-0040_1x04-1MP_P1.00mm_Horizontal" H 6950 5600 50  0001 C CNN
F 3 "~" H 6950 5600 50  0001 C CNN
	1    6950 5600
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0152
U 1 1 5E4DE117
P 7250 5800
F 0 "#PWR0152" H 7250 5650 50  0001 C CNN
F 1 "+5V" H 7265 5973 50  0000 C CNN
F 2 "" H 7250 5800 50  0001 C CNN
F 3 "" H 7250 5800 50  0001 C CNN
	1    7250 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 5800 7250 5700
Wire Wire Line
	7250 5700 7150 5700
Text GLabel 7300 5500 2    50   Input ~ 0
USB2_DP
Text GLabel 7300 5600 2    50   Input ~ 0
USB2_DN
Wire Wire Line
	7300 5500 7150 5500
Wire Wire Line
	7300 5600 7150 5600
$Comp
L power:GND #PWR0153
U 1 1 5E4F30EC
P 7350 5300
F 0 "#PWR0153" H 7350 5050 50  0001 C CNN
F 1 "GND" H 7355 5127 50  0000 C CNN
F 2 "" H 7350 5300 50  0001 C CNN
F 3 "" H 7350 5300 50  0001 C CNN
	1    7350 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 5400 7350 5400
Wire Wire Line
	7350 5400 7350 5300
Wire Notes Line
	6700 6250 7800 6250
Wire Notes Line
	7800 6250 7800 5000
Wire Notes Line
	7800 5000 6700 5000
Wire Notes Line
	6700 5000 6700 6250
$Comp
L Connector_Generic:Conn_01x10 J12
U 1 1 5E55B813
P 7450 3550
F 0 "J12" H 7530 3542 50  0000 L CNN
F 1 "GPIO" H 7250 2950 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical_SMD_Pin1Right" H 7450 3550 50  0001 C CNN
F 3 "~" H 7450 3550 50  0001 C CNN
	1    7450 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0154
U 1 1 5E5752A2
P 7150 4150
F 0 "#PWR0154" H 7150 3900 50  0001 C CNN
F 1 "GND" H 7155 3977 50  0000 C CNN
F 2 "" H 7150 4150 50  0001 C CNN
F 3 "" H 7150 4150 50  0001 C CNN
	1    7150 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0155
U 1 1 5E575C6C
P 7050 3950
F 0 "#PWR0155" H 7050 3800 50  0001 C CNN
F 1 "+5V" V 7065 4078 50  0000 L CNN
F 2 "" H 7050 3950 50  0001 C CNN
F 3 "" H 7050 3950 50  0001 C CNN
	1    7050 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7150 4150 7150 4050
Wire Wire Line
	7150 4050 7250 4050
Wire Wire Line
	7250 3950 7050 3950
Text GLabel 7250 3150 0    50   Input ~ 0
GPIO1
Text GLabel 7250 3250 0    50   Input ~ 0
GPIO2
Text GLabel 7250 3350 0    50   Input ~ 0
GPIO3
Text GLabel 7250 3450 0    50   Input ~ 0
GPIO4
Wire Notes Line
	6700 4950 7800 4950
Wire Notes Line
	7800 4950 7800 2250
Wire Notes Line
	7800 2250 6700 2250
Wire Notes Line
	6700 2250 6700 4950
Text Notes 6750 4900 0    50   ~ 0
GPIO
Text GLabel 2550 3550 2    50   Input ~ 0
USB3_DP
Text GLabel 2550 3450 2    50   Input ~ 0
USB3_DN
Wire Wire Line
	1150 6050 1400 6050
Wire Wire Line
	4250 6050 4500 6050
$Comp
L Connector:Micro_SD_Card_Det_Hirose_DM3AT J13
U 1 1 5E202588
P 9850 2050
F 0 "J13" H 9800 2867 50  0000 C CNN
F 1 "Micro_SD_Card_Det_Hirose_DM3AT" H 9800 2776 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3D-SF" H 11900 2750 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 9850 2150 50  0001 C CNN
	1    9850 2050
	1    0    0    -1  
$EndComp
Text GLabel 8750 1650 0    50   Input ~ 0
SD_DATA2
Text GLabel 8750 1750 0    50   Input ~ 0
SD_DATA3
Text GLabel 8750 1850 0    50   Input ~ 0
SD_CMD
Text GLabel 8750 2050 0    50   Input ~ 0
SD_CLK
Text GLabel 8750 2250 0    50   Input ~ 0
SD_DATA0
Text GLabel 8750 2350 0    50   Input ~ 0
SD_DATA1
NoConn ~ 8950 2450
NoConn ~ 8950 2550
Wire Wire Line
	8750 2350 8950 2350
Wire Wire Line
	8950 2250 8750 2250
Wire Wire Line
	8750 1750 8950 1750
Wire Wire Line
	8750 1650 8950 1650
Wire Wire Line
	8750 1850 8950 1850
Wire Wire Line
	8750 2050 8950 2050
$Comp
L power:GND #PWR0166
U 1 1 5E21D439
P 8750 2150
F 0 "#PWR0166" H 8750 1900 50  0001 C CNN
F 1 "GND" V 8755 2022 50  0000 R CNN
F 2 "" H 8750 2150 50  0001 C CNN
F 3 "" H 8750 2150 50  0001 C CNN
	1    8750 2150
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0167
U 1 1 5E220EFD
P 8750 1950
F 0 "#PWR0167" H 8750 1800 50  0001 C CNN
F 1 "+3V3" V 8765 2078 50  0000 L CNN
F 2 "" H 8750 1950 50  0001 C CNN
F 3 "" H 8750 1950 50  0001 C CNN
	1    8750 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 1950 8750 1950
Wire Wire Line
	8750 2150 8950 2150
$Comp
L power:GND #PWR0168
U 1 1 5E227CC2
P 10700 2650
F 0 "#PWR0168" H 10700 2400 50  0001 C CNN
F 1 "GND" H 10705 2477 50  0000 C CNN
F 2 "" H 10700 2650 50  0001 C CNN
F 3 "" H 10700 2650 50  0001 C CNN
	1    10700 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 2550 10700 2550
Wire Wire Line
	10700 2550 10700 2650
Wire Notes Line
	7850 3500 11000 3500
Wire Notes Line
	11000 3500 11000 800 
Wire Notes Line
	11000 800  7850 800 
Wire Notes Line
	7850 800  7850 3500
Text Notes 7900 3450 0    50   ~ 0
MicroSD Card (Always Powered)
Text GLabel 7250 3550 0    50   Input ~ 0
GPIO5
Text GLabel 7250 3650 0    50   Input ~ 0
GPIO6
Text GLabel 7250 3750 0    50   Input ~ 0
GPIO7
Text GLabel 7250 3850 0    50   Input ~ 0
GPIO8
$Comp
L Connector:HDMI_A J1
U 1 1 5ED4E247
P 2000 1500
F 0 "J1" V 2475 1500 50  0000 C CNN
F 1 "HDMI_A" V 2566 1500 50  0000 C CNN
F 2 "digikey-footprints:HDMI_A_Female_2000-1-2-41-00-BK" H 2025 1500 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/HDMI" H 2025 1500 50  0001 C CNN
	1    2000 1500
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR014
U 1 1 5ED54118
P 3250 1350
F 0 "#PWR014" H 3250 1200 50  0001 C CNN
F 1 "+5V" H 3265 1523 50  0000 C CNN
F 2 "" H 3250 1350 50  0001 C CNN
F 3 "" H 3250 1350 50  0001 C CNN
	1    3250 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1500 3250 1500
Wire Wire Line
	3250 1500 3250 1350
Text GLabel 1900 1100 1    50   Input ~ 0
HDMI_CEC
Text GLabel 1600 1100 1    50   Input ~ 0
HDMI_SDA
Text GLabel 1700 1100 1    50   Input ~ 0
HDMI_SCL
Text GLabel 2200 1100 1    50   Input ~ 0
HDMI_CLK_P
Text GLabel 2100 1100 1    50   Input ~ 0
HDMI_CLK_N
Text GLabel 2400 1100 1    50   Input ~ 0
HDMI_D0_P
Text GLabel 2300 1100 1    50   Input ~ 0
HDMI_D0_N
Text GLabel 2600 1100 1    50   Input ~ 0
HDMI_D1_P
Text GLabel 2500 1100 1    50   Input ~ 0
HDMI_D1_N
Text GLabel 2800 1100 1    50   Input ~ 0
HDMI_D2_P
Text GLabel 2700 1100 1    50   Input ~ 0
HDMI_D2_N
Text GLabel 1300 1100 1    50   Input ~ 0
HDMI_HPD
$Comp
L power:GND #PWR013
U 1 1 5EDB6202
P 750 1900
F 0 "#PWR013" H 750 1650 50  0001 C CNN
F 1 "GND" H 755 1727 50  0000 C CNN
F 2 "" H 750 1900 50  0001 C CNN
F 3 "" H 750 1900 50  0001 C CNN
	1    750  1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  1700 750  1700
Wire Wire Line
	750  1700 750  1800
Wire Wire Line
	900  1800 750  1800
Connection ~ 750  1800
Wire Wire Line
	750  1800 750  1900
Wire Notes Line
	550  2200 550  500 
Wire Notes Line
	550  500  3550 500 
Wire Notes Line
	3550 500  3550 2200
Wire Notes Line
	3550 2200 550  2200
NoConn ~ 900  1300
NoConn ~ 900  1400
NoConn ~ 900  1500
NoConn ~ 900  1600
$Comp
L Connector:Micro_SD_Card_Det_Hirose_DM3AT J3
U 1 1 5E5AE19E
P 9850 4800
F 0 "J3" H 9800 5617 50  0000 C CNN
F 1 "Micro_SD_Card_Det_Hirose_DM3AT" H 9800 5526 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3D-SF" H 11900 5500 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 9850 4900 50  0001 C CNN
	1    9850 4800
	1    0    0    -1  
$EndComp
Text GLabel 8750 4400 0    50   Input ~ 0
SD1_DATA2
Text GLabel 8750 4500 0    50   Input ~ 0
SD1_DATA3
Text GLabel 8750 4600 0    50   Input ~ 0
SD1_CMD
Text GLabel 8750 4800 0    50   Input ~ 0
SD1_CLK
Text GLabel 8750 5000 0    50   Input ~ 0
SD1_DATA0
Text GLabel 8750 5100 0    50   Input ~ 0
SD1_DATA1
NoConn ~ 8950 5200
NoConn ~ 8950 5300
Wire Wire Line
	8750 5100 8950 5100
Wire Wire Line
	8950 5000 8750 5000
Wire Wire Line
	8750 4500 8950 4500
Wire Wire Line
	8750 4400 8950 4400
Wire Wire Line
	8750 4600 8950 4600
Wire Wire Line
	8750 4800 8950 4800
$Comp
L power:GND #PWR0101
U 1 1 5E5AE1B2
P 8750 4900
F 0 "#PWR0101" H 8750 4650 50  0001 C CNN
F 1 "GND" V 8755 4772 50  0000 R CNN
F 2 "" H 8750 4900 50  0001 C CNN
F 3 "" H 8750 4900 50  0001 C CNN
	1    8750 4900
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 5E5AE1B8
P 8750 4700
F 0 "#PWR0102" H 8750 4550 50  0001 C CNN
F 1 "+3V3" V 8765 4828 50  0000 L CNN
F 2 "" H 8750 4700 50  0001 C CNN
F 3 "" H 8750 4700 50  0001 C CNN
	1    8750 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 4700 8750 4700
Wire Wire Line
	8750 4900 8950 4900
$Comp
L power:GND #PWR0103
U 1 1 5E5AE1C0
P 10700 5400
F 0 "#PWR0103" H 10700 5150 50  0001 C CNN
F 1 "GND" H 10705 5227 50  0000 C CNN
F 2 "" H 10700 5400 50  0001 C CNN
F 3 "" H 10700 5400 50  0001 C CNN
	1    10700 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 5300 10700 5300
Wire Wire Line
	10700 5300 10700 5400
Wire Notes Line
	7850 6250 11000 6250
Wire Notes Line
	11000 6250 11000 3550
Wire Notes Line
	11000 3550 7850 3550
Wire Notes Line
	7850 3550 7850 6250
Text Notes 7900 6200 0    50   ~ 0
2nd MicroSD Card (Always Powered) (May not be populated)
NoConn ~ 1400 1100
$EndSCHEMATC
