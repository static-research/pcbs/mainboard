EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+1V8 #PWR07
U 1 1 5E633911
P 1250 2650
F 0 "#PWR07" H 1250 2500 50  0001 C CNN
F 1 "+1V8" V 1265 2778 50  0000 L CNN
F 2 "" H 1250 2650 50  0001 C CNN
F 3 "" H 1250 2650 50  0001 C CNN
	1    1250 2650
	0    -1   -1   0   
$EndComp
Text GLabel 3100 3500 3    50   BiDi ~ 0
USB1_DP
Text GLabel 3200 3500 3    50   BiDi ~ 0
USB1_DN
Wire Wire Line
	3100 3350 3100 3500
Wire Wire Line
	3200 3500 3200 3350
NoConn ~ 3000 3350
Text GLabel 3400 3500 3    50   Input ~ 0
PCLK
Text GLabel 3500 3500 3    50   Input ~ 0
DE
Text GLabel 3600 3500 3    50   Input ~ 0
LCD_VSYNC
Text GLabel 3700 3500 3    50   Input ~ 0
LCD_HSYNC
Text GLabel 3800 3500 3    50   Input ~ 0
DPI_B2
Text GLabel 3900 3500 3    50   Input ~ 0
DPI_B3
Text GLabel 4000 3500 3    50   Input ~ 0
DPI_B4
Text GLabel 4100 3500 3    50   Input ~ 0
DPI_B5
Text GLabel 4200 3500 3    50   Input ~ 0
DPI_B6
Text GLabel 4300 3500 3    50   Input ~ 0
DPI_B7
Text GLabel 4400 3500 3    50   Input ~ 0
DPI_G2
Text GLabel 4500 3500 3    50   Input ~ 0
DPI_G3
Text GLabel 4600 3500 3    50   Input ~ 0
DPI_G4
$Comp
L power:+3V3 #PWR06
U 1 1 5E64E68E
P 1250 2550
F 0 "#PWR06" H 1250 2400 50  0001 C CNN
F 1 "+3V3" V 1265 2678 50  0000 L CNN
F 2 "" H 1250 2550 50  0001 C CNN
F 3 "" H 1250 2550 50  0001 C CNN
	1    1250 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 2650 1600 2650
Wire Wire Line
	1600 2550 1250 2550
$Comp
L power:+3V3 #PWR04
U 1 1 5E65A5F5
P 1250 2250
F 0 "#PWR04" H 1250 2100 50  0001 C CNN
F 1 "+3V3" V 1265 2378 50  0000 L CNN
F 2 "" H 1250 2250 50  0001 C CNN
F 3 "" H 1250 2250 50  0001 C CNN
	1    1250 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 2250 1600 2250
Wire Wire Line
	1250 2150 1600 2150
$Comp
L power:+3V3 #PWR03
U 1 1 5E6686F7
P 1250 2150
F 0 "#PWR03" H 1250 2000 50  0001 C CNN
F 1 "+3V3" V 1265 2278 50  0000 L CNN
F 2 "" H 1250 2150 50  0001 C CNN
F 3 "" H 1250 2150 50  0001 C CNN
	1    1250 2150
	0    -1   -1   0   
$EndComp
$Comp
L MCU_Module:RaspberryPi-CM3+L U8
U 1 1 5E58AAEB
P 4900 2350
F 0 "U8" H 4900 -1039 50  0000 C CNN
F 1 "RaspberryPi-CM3+L" H 4900 -1130 50  0000 C CNN
F 2 "PCB:TE_1473149-4" H 4300 5750 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/computemodule/datasheets/rpi_DATA_CM3plus_1p0.pdf" H 4300 5750 50  0001 C CNN
	1    4900 2350
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR02
U 1 1 5E678EB0
P 1250 2050
F 0 "#PWR02" H 1250 1900 50  0001 C CNN
F 1 "+3V3" V 1265 2178 50  0000 L CNN
F 2 "" H 1250 2050 50  0001 C CNN
F 3 "" H 1250 2050 50  0001 C CNN
	1    1250 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 2050 1600 2050
Wire Wire Line
	1250 1950 1600 1950
$Comp
L power:+3V3 #PWR01
U 1 1 5E678EB8
P 1250 1950
F 0 "#PWR01" H 1250 1800 50  0001 C CNN
F 1 "+3V3" V 1265 2078 50  0000 L CNN
F 2 "" H 1250 1950 50  0001 C CNN
F 3 "" H 1250 1950 50  0001 C CNN
	1    1250 1950
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5E681C7B
P 1250 2450
F 0 "#PWR05" H 1250 2300 50  0001 C CNN
F 1 "+5V" V 1265 2578 50  0000 L CNN
F 2 "" H 1250 2450 50  0001 C CNN
F 3 "" H 1250 2450 50  0001 C CNN
	1    1250 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 2450 1250 2450
Text GLabel 2700 3500 3    50   Input ~ 0
SD_DATA2
Text GLabel 2800 3500 3    50   Input ~ 0
SD_DATA3
Text GLabel 2400 3500 3    50   Input ~ 0
SD_CMD
Text GLabel 2300 3500 3    50   Input ~ 0
SD_CLK
Text GLabel 2500 3500 3    50   Input ~ 0
SD_DATA0
Text GLabel 2600 3500 3    50   Input ~ 0
SD_DATA1
$Comp
L Connector:TestPoint TP2
U 1 1 5E6B09E0
P 1550 3450
F 0 "TP2" V 1745 3522 50  0000 C CNN
F 1 "RUN" V 1654 3522 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1750 3450 50  0001 C CNN
F 3 "~" H 1750 3450 50  0001 C CNN
	1    1550 3450
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5E6B8BD5
P 1550 3750
F 0 "TP4" V 1757 3822 50  0000 C CNN
F 1 "~EMMC_DISABLE" V 1659 3822 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1750 3750 50  0001 C CNN
F 3 "~" H 1750 3750 50  0001 C CNN
	1    1550 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 3450 1900 3450
Wire Wire Line
	1900 3450 1900 3350
Wire Wire Line
	1550 3750 2000 3750
Wire Wire Line
	2000 3750 2000 3350
NoConn ~ 2100 3350
Wire Wire Line
	2300 3500 2300 3350
Wire Wire Line
	2400 3500 2400 3350
Wire Wire Line
	2500 3500 2500 3350
Wire Wire Line
	2600 3500 2600 3350
Wire Wire Line
	2700 3500 2700 3350
Wire Wire Line
	2800 3500 2800 3350
Wire Wire Line
	3400 3500 3400 3350
Wire Wire Line
	3500 3500 3500 3350
Wire Wire Line
	3600 3500 3600 3350
Wire Wire Line
	3700 3500 3700 3350
Wire Wire Line
	3800 3500 3800 3350
Wire Wire Line
	3900 3500 3900 3350
Wire Wire Line
	4000 3500 4000 3350
Wire Wire Line
	4100 3500 4100 3350
Wire Wire Line
	4200 3500 4200 3350
Wire Wire Line
	4300 3500 4300 3350
Wire Wire Line
	4400 3500 4400 3350
Wire Wire Line
	4500 3500 4500 3350
Wire Wire Line
	4600 3500 4600 3350
Text GLabel 4700 3500 3    50   Input ~ 0
DPI_G5
Text GLabel 4800 3500 3    50   Input ~ 0
DPI_G6
Text GLabel 4900 3500 3    50   Input ~ 0
DPI_G7
Text GLabel 5000 3500 3    50   Input ~ 0
DPI_R2
Text GLabel 5100 3500 3    50   Input ~ 0
DPI_R3
Text GLabel 5200 3500 3    50   Input ~ 0
DPI_R4
Text GLabel 5300 3500 3    50   Input ~ 0
DPI_R5
Text GLabel 5400 3500 3    50   Input ~ 0
DPI_R6
Text GLabel 5500 3500 3    50   Input ~ 0
DPI_R7
Wire Wire Line
	4700 3500 4700 3350
Wire Wire Line
	4800 3500 4800 3350
Wire Wire Line
	4900 3500 4900 3350
Wire Wire Line
	5000 3500 5000 3350
Wire Wire Line
	5100 3500 5100 3350
Wire Wire Line
	5200 3500 5200 3350
Wire Wire Line
	5300 3500 5300 3350
Wire Wire Line
	5400 3500 5400 3350
Wire Wire Line
	5500 3500 5500 3350
NoConn ~ 2400 1350
NoConn ~ 2500 1350
NoConn ~ 2600 1350
NoConn ~ 2700 1350
NoConn ~ 2800 1350
NoConn ~ 2900 1350
NoConn ~ 3100 1350
NoConn ~ 3200 1350
NoConn ~ 3300 1350
NoConn ~ 3400 1350
NoConn ~ 3500 1350
NoConn ~ 3600 1350
NoConn ~ 3700 1350
NoConn ~ 3800 1350
NoConn ~ 3900 1350
NoConn ~ 4000 1350
NoConn ~ 5600 1350
NoConn ~ 5700 1350
NoConn ~ 5800 1350
NoConn ~ 5900 1350
NoConn ~ 6000 1350
NoConn ~ 6100 1350
NoConn ~ 6300 1350
NoConn ~ 6400 1350
NoConn ~ 6500 1350
NoConn ~ 6600 1350
NoConn ~ 6700 1350
NoConn ~ 6800 1350
NoConn ~ 6900 1350
NoConn ~ 7000 1350
NoConn ~ 7100 1350
NoConn ~ 7200 1350
NoConn ~ 7500 1350
NoConn ~ 7600 1350
NoConn ~ 7700 1350
NoConn ~ 7800 1350
NoConn ~ 7900 1350
Text GLabel 5600 3500 3    50   Input ~ 0
GPIO1
Text GLabel 5700 3500 3    50   Input ~ 0
GPIO2
Text GLabel 5800 3500 3    50   Input ~ 0
GPIO3
Text GLabel 5900 3500 3    50   Input ~ 0
GPIO4
Text GLabel 6000 3500 3    50   Input ~ 0
GPIO5
Text GLabel 6100 3500 3    50   Input ~ 0
GPIO6
Text GLabel 6200 3500 3    50   Input ~ 0
GPIO7
Text GLabel 6300 3500 3    50   Input ~ 0
GPIO8
Wire Wire Line
	5600 3500 5600 3350
Wire Wire Line
	5700 3500 5700 3350
Wire Wire Line
	5800 3500 5800 3350
Wire Wire Line
	5900 3500 5900 3350
Wire Wire Line
	6000 3500 6000 3350
Wire Wire Line
	6100 3500 6100 3350
Wire Wire Line
	6200 3500 6200 3350
Wire Wire Line
	6300 3500 6300 3350
Text GLabel 4200 1350 1    50   Input ~ 0
HDMI_CEC
Text GLabel 4300 1350 1    50   Input ~ 0
HDMI_SDA
Text GLabel 4400 1350 1    50   Input ~ 0
HDMI_SCL
Text GLabel 4500 1350 1    50   Input ~ 0
HDMI_CLK_P
Text GLabel 4600 1350 1    50   Input ~ 0
HDMI_CLK_N
Text GLabel 4700 1350 1    50   Input ~ 0
HDMI_D0_P
Text GLabel 4800 1350 1    50   Input ~ 0
HDMI_D0_N
Text GLabel 4900 1350 1    50   Input ~ 0
HDMI_D1_P
Text GLabel 5000 1350 1    50   Input ~ 0
HDMI_D1_N
Text GLabel 5100 1350 1    50   Input ~ 0
HDMI_D2_P
Text GLabel 5200 1350 1    50   Input ~ 0
HDMI_D2_N
Text GLabel 5300 1350 1    50   Input ~ 0
HDMI_HPD
Text GLabel 6700 3350 3    50   Input ~ 0
UART_1_RXD
Text GLabel 6600 3350 3    50   Input ~ 0
UART1_TXD
Text GLabel 7200 3350 3    50   Input ~ 0
SD1_DATA2
Text GLabel 7300 3350 3    50   Input ~ 0
SD1_DATA3
Text GLabel 6900 3350 3    50   Input ~ 0
SD1_CMD
Text GLabel 6800 3350 3    50   Input ~ 0
SD1_CLK
Text GLabel 7000 3350 3    50   Input ~ 0
SD1_DATA0
Text GLabel 7100 3350 3    50   Input ~ 0
SD1_DATA1
NoConn ~ 6400 3350
NoConn ~ 6500 3350
NoConn ~ 7400 3350
NoConn ~ 7500 3350
NoConn ~ 7600 3350
NoConn ~ 7700 3350
NoConn ~ 7800 3350
NoConn ~ 7900 3350
$Comp
L power:GND #PWR08
U 1 1 5EE3E7E2
P 8250 2550
F 0 "#PWR08" H 8250 2300 50  0001 C CNN
F 1 "GND" H 8255 2377 50  0000 C CNN
F 2 "" H 8250 2550 50  0001 C CNN
F 3 "" H 8250 2550 50  0001 C CNN
	1    8250 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2350 8250 2350
Wire Wire Line
	8250 2350 8250 2550
NoConn ~ 2200 1350
Wire Notes Line
	750  4250 8550 4250
Wire Notes Line
	8550 4250 8550 650 
Wire Notes Line
	8550 650  750  650 
Wire Notes Line
	750  650  750  4250
Text Notes 800  4200 0    50   ~ 0
Raspberry Pi Compute Module
$EndSCHEMATC
