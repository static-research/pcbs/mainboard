EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_C_Receptacle_USB2.0 J5
U 1 1 5D9E2566
P 1200 4900
F 0 "J5" H 1307 5767 50  0000 C CNN
F 1 "Debug USB" H 1307 5676 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_Palconn_UTC16-G" H 1350 4900 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1350 4900 50  0001 C CNN
	1    1200 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5D9E4F3F
P 1050 6000
F 0 "#PWR0110" H 1050 5750 50  0001 C CNN
F 1 "GND" H 1055 5827 50  0000 C CNN
F 2 "" H 1050 6000 50  0001 C CNN
F 3 "" H 1050 6000 50  0001 C CNN
	1    1050 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5800 1200 5900
Wire Wire Line
	1200 5900 1050 5900
Wire Wire Line
	1050 5900 1050 6000
Wire Wire Line
	900  5800 900  5900
Wire Wire Line
	900  5900 1050 5900
Connection ~ 1050 5900
NoConn ~ 1800 5400
NoConn ~ 1800 5500
$Comp
L Device:R_US R1
U 1 1 5D9ECED9
P 2100 4550
F 0 "R1" V 1895 4550 50  0000 C CNN
F 1 "5.1K Ohm" V 1986 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2140 4540 50  0001 C CNN
F 3 "~" H 2100 4550 50  0001 C CNN
	1    2100 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4600 1900 4600
Wire Wire Line
	1900 4600 1900 4550
Wire Wire Line
	1900 4550 1950 4550
Wire Wire Line
	1900 4550 1900 4500
Wire Wire Line
	1900 4500 1800 4500
Connection ~ 1900 4550
Connection ~ 1200 5900
$Comp
L Interface_USB:CH330N U4
U 1 1 5E15C8EB
P 3050 5150
F 0 "U4" H 2850 5400 50  0000 C CNN
F 1 "CH330N" H 3200 5400 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2900 5900 50  0001 C CNN
F 3 "http://www.wch.cn/downloads/file/240.html" H 2950 5350 50  0001 C CNN
	1    3050 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4300 2350 4300
Wire Wire Line
	3050 5550 3050 5900
Text GLabel 3750 5050 2    50   Input ~ 0
UART_1_RXD
Text GLabel 3750 5150 2    50   Input ~ 0
UART1_TXD
$Comp
L Jumper:SolderJumper_2_Bridged JP5
U 1 1 5E18B05B
P 3600 5150
F 0 "JP5" H 3900 4950 50  0000 C CNN
F 1 "UART1_TXD" H 3600 5050 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 3600 5150 50  0001 C CNN
F 3 "~" H 3600 5150 50  0001 C CNN
	1    3600 5150
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP4
U 1 1 5E18DA02
P 3600 5050
F 0 "JP4" H 3600 5255 50  0000 C CNN
F 1 "UART1_RXD" H 3600 5150 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 3600 5050 50  0001 C CNN
F 3 "~" H 3600 5050 50  0001 C CNN
	1    3600 5050
	1    0    0    -1  
$EndComp
Text GLabel 2650 5250 0    50   Input ~ 0
DebugUSB_D+
Text GLabel 2650 5350 0    50   Input ~ 0
DebugUSB_D-
Text GLabel 1900 5000 2    50   Input ~ 0
DebugUSB_D+
Text GLabel 1900 4800 2    50   Input ~ 0
DebugUSB_D-
Wire Wire Line
	1800 5100 1850 5100
Wire Wire Line
	1850 5100 1850 5000
Wire Wire Line
	1800 5000 1850 5000
Wire Wire Line
	1900 4800 1800 4800
Wire Wire Line
	1800 4900 1800 4800
Connection ~ 1800 4800
NoConn ~ 2650 5050
NoConn ~ 3450 5350
$Comp
L Device:C_Small C8
U 1 1 5E1A54CD
P 2350 4400
F 0 "C8" H 2442 4446 50  0000 L CNN
F 1 "0.1uF" H 2442 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2350 4400 50  0001 C CNN
F 3 "~" H 2350 4400 50  0001 C CNN
	1    2350 4400
	1    0    0    -1  
$EndComp
Connection ~ 2350 4300
Wire Wire Line
	2350 4300 1800 4300
$Comp
L power:GND #PWR0139
U 1 1 5E1A6968
P 2450 4550
F 0 "#PWR0139" H 2450 4300 50  0001 C CNN
F 1 "GND" V 2455 4422 50  0000 R CNN
F 2 "" H 2450 4550 50  0001 C CNN
F 3 "" H 2450 4550 50  0001 C CNN
	1    2450 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 4550 2350 4550
Wire Wire Line
	2350 4550 2350 4500
Wire Wire Line
	1200 5900 3050 5900
$Comp
L power:GND #PWR0140
U 1 1 5E1AC0CF
P 2250 4550
F 0 "#PWR0140" H 2250 4300 50  0001 C CNN
F 1 "GND" H 2255 4377 50  0000 C CNN
F 2 "" H 2250 4550 50  0001 C CNN
F 3 "" H 2250 4550 50  0001 C CNN
	1    2250 4550
	1    0    0    -1  
$EndComp
Wire Notes Line
	700  6600 700  3800
Wire Notes Line
	700  3800 5250 3800
Wire Notes Line
	5250 6600 700  6600
Text Notes 750  6550 0    50   ~ 0
USB Debug Port
Wire Wire Line
	3050 4300 3050 4850
Wire Wire Line
	1900 5000 1850 5000
Connection ~ 1850 5000
Wire Notes Line
	5250 3800 5250 6600
Text Notes 750  3700 0    50   ~ 0
Debug UART Header
Wire Notes Line
	700  1950 700  3750
Wire Notes Line
	2900 1950 700  1950
Wire Notes Line
	2900 3750 2900 1950
Wire Notes Line
	700  3750 2900 3750
Wire Wire Line
	1700 2650 1550 2650
Wire Wire Line
	1700 2500 1700 2650
$Comp
L power:+3.3V #PWR0112
U 1 1 5DAC041E
P 1700 2500
F 0 "#PWR0112" H 1700 2350 50  0001 C CNN
F 1 "+3.3V" H 1715 2673 50  0000 C CNN
F 2 "" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2950 1650 3050
Wire Wire Line
	1550 2950 1650 2950
$Comp
L power:GND #PWR0111
U 1 1 5DABD06E
P 1650 3050
F 0 "#PWR0111" H 1650 2800 50  0001 C CNN
F 1 "GND" H 1655 2877 50  0000 C CNN
F 2 "" H 1650 3050 50  0001 C CNN
F 3 "" H 1650 3050 50  0001 C CNN
	1    1650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2850 1550 2850
Wire Wire Line
	1850 2750 1550 2750
Text GLabel 1850 2850 2    50   Input ~ 0
UART1_TXD
Text GLabel 1850 2750 2    50   Input ~ 0
UART_1_RXD
$Comp
L Connector:Conn_01x04_Male J6
U 1 1 5DAAB00F
P 1350 2750
F 0 "J6" H 1458 3031 50  0000 C CNN
F 1 "Debug UART" H 1458 2940 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1350 2750 50  0001 C CNN
F 3 "~" H 1350 2750 50  0001 C CNN
	1    1350 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
